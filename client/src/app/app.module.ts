import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CollegeDetailFormComponent } from './components/college/college-detail-form/college-detail-form.component';
import { CollegeDetailViewComponent } from './components/college/college-detail-view/college-detail-view.component';
import { CompanyDetailFormComponent } from './components/company/company-detail-form/company-detail-form.component';
import { CompanyDetailViewComponent } from './components/company/company-detail-view/company-detail-view.component';
import { LocationComponent } from './components/dropdowns/location/location.component';
import { QualificationComponent } from './components/dropdowns/qualification/qualification.component';
import { SkillsetComponent } from './components/dropdowns/skillset/skillset.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { NavbarComponent } from './components/home-page/navbar/navbar.component';
import { JobDetailFormComponent } from './components/job/job-detail-form/job-detail-form.component';
import { JobDetailViewComponent } from './components/job/job-detail-view/job-detail-view.component';
import { LandingPageHeaderComponent } from './components/landing-page/landing-page-header/landing-page-header.component';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { LoginComponent } from './components/login/login.component';
import { CollegeProfileComponent } from './components/profiles/college-profile/college-profile.component';
import { CompanyProfileComponent } from './components/profiles/company-profile/company-profile.component';
import { UserProfileComponent } from './components/profiles/user-profile/user-profile.component';
import { RegisterComponent } from './components/register/register.component';
import { UserDetailFormComponent } from './components/user/user-detail-form/user-detail-form.component';
import { UserDetailViewComponent } from './components/user/user-detail-view/user-detail-view.component';
@NgModule({
  declarations: [
    AppComponent,
    LandingPageComponent,
    LandingPageHeaderComponent,
    LoginComponent,
    RegisterComponent,
    HomePageComponent,
    NavbarComponent,
    UserProfileComponent,
    CollegeProfileComponent,
    CompanyProfileComponent,
    JobDetailViewComponent,
    JobDetailFormComponent,
    QualificationComponent,
    SkillsetComponent,
    LocationComponent,
    CollegeDetailViewComponent,
    CollegeDetailFormComponent,
    CompanyDetailFormComponent,
    CompanyDetailViewComponent,
    UserDetailViewComponent,
    UserDetailFormComponent,
  ],
  imports: [BrowserModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
